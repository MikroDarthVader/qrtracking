﻿
using System;
using System.Threading.Tasks;


#if ENABLE_WINMD_SUPPORT

using Windows.Graphics.Imaging;
using Windows.Foundation;
using Windows.Foundation.Diagnostics;
using Windows.Perception.Spatial;

// Include winrt components
using HoloLensForCV;
#endif

using UnityEngine;

using ZXing;
using ZXing.QrCode;
using ZXing.Common;
using System.Runtime.InteropServices;
using System.Threading;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;
using Vector4 = System.Numerics.Vector4;
using Matrix4x4 = System.Numerics.Matrix4x4;

public class QrCodeDetector : MonoBehaviour
{
    public int depthRangeMin;
    public int depthRangeMax;

    public GameObject sphere;
    public Camera cam;

    private Material sphereMaterial;
    private GameObject onQrCodeSphere;

#if ENABLE_WINMD_SUPPORT
    // Enable winmd support to include winmd files. Will not
    // run in Unity editor.

    private bool inited = false;
    private SensorFrameStreamer sensorFrameStreamerRGB;
    private SensorFrameStreamer sensorFrameStreamerShortDepth;
    private SensorFrameStreamer sensorFrameStreamerLongDepth;
    private SpatialPerception spatialPerception;

    private MediaFrameSourceGroup mediaFrameSourceGroupRGB;
    private MediaFrameSourceGroup mediaFrameSourceGroupShortDepth;
    private MediaFrameSourceGroup mediaFrameSourceGroupLongDepth;

    private DepthPvMapperNew depthPvMapper;

    LoggingChannel lc_init;
    LoggingChannel lc_update;
#endif

    //zxing
    private QRCodeReader qrCodeReader;

    #region Unity
    async void Start()
    {
#if ENABLE_WINMD_SUPPORT
        lc_init = new LoggingChannel("Init", null, new Guid("4bd2826e-54a1-4ba9-bf63-92b73ea1ac4a"));
        lc_update = new LoggingChannel("Update", null, new Guid("4bd2826e-54a1-4ba9-bf63-92b73ea1ac4a"));
        lc_init.LogMessage("Initializating started", LoggingLevel.Information);
#endif

        if (depthRangeMin > depthRangeMax)
        {
#if ENABLE_WINMD_SUPPORT
            lc_init.LogMessage("Not valid depth range", LoggingLevel.Critical);
#endif
#if UNITY_EDITOR
            Debug.LogError("Not valid depth range");
#endif
            return;
        }

        onQrCodeSphere = Instantiate(sphere);
        onQrCodeSphere.transform.position = new Vector3(0, 0, 1);
        sphereMaterial = onQrCodeSphere.GetComponent<MeshRenderer>().material;

        qrCodeReader = new QRCodeReader();

#if ENABLE_WINMD_SUPPORT

        await StartHoloLensMediaFrameSourceGroups();
#endif
    }

    void Update()
    {
#if ENABLE_WINMD_SUPPORT
        if (inited)
            UpdateHoloLensMediaFrameSourceGroup();
#endif
    }

    async void OnApplicationQuit()
    {
        await StopHoloLensMediaFrameSourceGroup();
    }

    #endregion

    async Task StartHoloLensMediaFrameSourceGroups()
    {
#if ENABLE_WINMD_SUPPORT
        // rgb
        sensorFrameStreamerRGB = new SensorFrameStreamer();
        sensorFrameStreamerRGB.Enable(SensorType.PhotoVideo);

        // Short Depth
        sensorFrameStreamerShortDepth = new SensorFrameStreamer();
        sensorFrameStreamerShortDepth.Enable(SensorType.ShortThrowToFDepth);

        // Short Depth
        sensorFrameStreamerLongDepth = new SensorFrameStreamer();
        sensorFrameStreamerLongDepth.Enable(SensorType.LongThrowToFDepth);

        lc_init.LogMessage("Sensor streamers inited", LoggingLevel.Information);

        // Spatial perception
        spatialPerception = new SpatialPerception();

        lc_init.LogMessage("Spatial perception inited", LoggingLevel.Information);

        // Enable media frame source groups
        // RGB
        mediaFrameSourceGroupRGB = new MediaFrameSourceGroup(MediaFrameSourceGroupType.PhotoVideoCamera,
                                                             spatialPerception,
                                                             sensorFrameStreamerRGB);
        mediaFrameSourceGroupRGB.Enable(SensorType.PhotoVideo);

        // Short Depth
        mediaFrameSourceGroupShortDepth = new MediaFrameSourceGroup(MediaFrameSourceGroupType.HoloLensResearchModeSensors,
                                                                    spatialPerception,
                                                                    sensorFrameStreamerShortDepth);
        mediaFrameSourceGroupShortDepth.Enable(SensorType.ShortThrowToFDepth);

        // Long Depth
        mediaFrameSourceGroupLongDepth = new MediaFrameSourceGroup(MediaFrameSourceGroupType.HoloLensResearchModeSensors,
                                                                    spatialPerception,
                                                                    sensorFrameStreamerLongDepth);
        mediaFrameSourceGroupLongDepth.Enable(SensorType.LongThrowToFDepth);

        lc_init.LogMessage("Media source groups inited", LoggingLevel.Information);

        // Start media frame source groups
        await mediaFrameSourceGroupRGB.StartAsync();
        await mediaFrameSourceGroupShortDepth.StartAsync();
        await mediaFrameSourceGroupLongDepth.StartAsync();

        lc_init.LogMessage("Media source groups started", LoggingLevel.Information);

        SensorFrame shortDepthFrame = mediaFrameSourceGroupShortDepth.GetLatestSensorFrame(SensorType.ShortThrowToFDepth);
        if (shortDepthFrame == null)
        {
            lc_init.LogMessage("shortDepthFrame is null", LoggingLevel.Error);
            return;
        }

        depthPvMapper = new DepthPvMapperNew(shortDepthFrame);

        lc_init.LogMessage("Depth PV mapper inited", LoggingLevel.Information);

        inited = true;

        lc_init.LogMessage("Initializating ended", LoggingLevel.Information);
#endif
    }

    long updateCounter = -1;
    long lastFrameTicks;
    unsafe void UpdateHoloLensMediaFrameSourceGroup()
    {
#if ENABLE_WINMD_SUPPORT
        lc_update.LogMessage(++updateCounter + " frame", LoggingLevel.Information);

        // Get latest sensor frames
        SensorFrame latestRGBCameraFrame =
            mediaFrameSourceGroupRGB.GetLatestSensorFrame(SensorType.PhotoVideo);
        if (latestRGBCameraFrame == null)
        {
            lc_update.LogMessage("latestRGBCameraFrame is null", LoggingLevel.Error);
            return;
        }

        lc_update.LogMessage("latestRGBCameraFrame.Timestamp: " + latestRGBCameraFrame.Timestamp.Ticks.ToString(), LoggingLevel.Information);

        if (latestRGBCameraFrame.Timestamp.Ticks != lastFrameTicks)
            lastFrameTicks = latestRGBCameraFrame.Timestamp.Ticks;
        else
        {
            lc_update.LogMessage("RGB frame not changed", LoggingLevel.Information);
            return;
        }

        SensorFrame latestShortDepthCameraFrame =
            mediaFrameSourceGroupShortDepth.GetLatestSensorFrame(SensorType.ShortThrowToFDepth);
        if (latestShortDepthCameraFrame == null)
        {
            lc_update.LogMessage("latestShortDepthCameraFrame is null", LoggingLevel.Error);
            return;
        }

        lc_update.LogMessage("latestShortDepthCameraFrame.Timestamp: " + latestShortDepthCameraFrame.Timestamp.Ticks.ToString(), LoggingLevel.Information);

        SensorFrame latestLongDepthCameraFrame =
            mediaFrameSourceGroupLongDepth.GetLatestSensorFrame(SensorType.LongThrowToFDepth);
        if (latestLongDepthCameraFrame == null)
        {
            lc_update.LogMessage("latestLongDepthCameraFrame is null", LoggingLevel.Error);
            return;
        }

        lc_update.LogMessage("latestLongDepthCameraFrame.Timestamp: " + latestLongDepthCameraFrame.Timestamp.Ticks.ToString(), LoggingLevel.Information);

        var image = new Color32[latestRGBCameraFrame.SoftwareBitmap.PixelHeight * latestRGBCameraFrame.SoftwareBitmap.PixelWidth];
        var byteImage = GetByteArrayFromSoftwareBitmap(latestRGBCameraFrame.SoftwareBitmap);

        for (int i = 0; i < image.Length; i++)
            image[i] = new Color32(byteImage[i * 4], byteImage[i * 4 + 1], byteImage[i * 4 + 2], byteImage[i * 4 + 3]);

        lc_update.LogMessage("Middle rgb pixel: (" + image[image.Length / 2].r + ", " + image[image.Length / 2].g + ", " + image[image.Length / 2].b + ", " + image[image.Length / 2].a + ")", LoggingLevel.Information);

        var luminiance = new Color32LuminanceSource(image, latestRGBCameraFrame.SoftwareBitmap.PixelWidth,
                                                            latestRGBCameraFrame.SoftwareBitmap.PixelHeight);
        var binarizer = new HybridBinarizer(luminiance);
        var binBitmap = new BinaryBitmap(binarizer);
        var data = qrCodeReader.decode(binBitmap);

        if (data != null && data.ResultPoints.Length >= 3)
        {
            lc_update.LogMessage("Qr code detected", LoggingLevel.Information);
            sphereMaterial.color = Color.blue;

            // Map depth frames to photo video camera
            // with from/to range and specified radius.
            SensorFrame mappedDepthFrame = depthPvMapper.MapDepthToPV(latestRGBCameraFrame, latestShortDepthCameraFrame,
                                                                        latestLongDepthCameraFrame, depthRangeMin, depthRangeMax);
            if (mappedDepthFrame == null)
            {
                lc_update.LogMessage("mappedDepthFrame is null", LoggingLevel.Error);
                return;
            }

            ushort* mappedDepth = (ushort*)GetByteArrayFromSoftwareBitmap(mappedDepthFrame.SoftwareBitmap);
            Vector3 inWorldPoint = GetQrCodeWorldPosBy(latestRGBCameraFrame, data.ResultPoints, mappedDepth);

            if (inWorldPoint == Vector3.positiveInfinity)
                return;

            lc_update.LogMessage("In world point: <" + inWorldPoint.x + ", " + inWorldPoint.y + ", " + inWorldPoint.z + ">", LoggingLevel.Information);

            onQrCodeSphere.transform.position = inWorldPoint;
        }
        else
            sphereMaterial.color = Color.red;
#endif
        return;
    }

#if ENABLE_WINMD_SUPPORT
    unsafe Vector3 GetQrCodeWorldPosBy(SensorFrame rgbFrame, ResultPoint[] points, ushort* mappedDepth)
    {
        lc_update.LogMessage("Points: " + points[0].ToString() + "; " + points[1].ToString() + "; " + points[2].ToString(), LoggingLevel.Information);
        Vector2 middlePoint = new Vector2((points[0].X + points[2].X) / 2, (points[0].Y + points[2].Y) / 2);
        Vector2Int rouMidPoint = new Vector2Int((int)Math.Round(middlePoint.x), (int)Math.Round(middlePoint.y));

        bool depthCoordFounded = false;
        Vector2Int nearestDepthCoord = Vector2Int.zero;

        if (GetPixelDepth(mappedDepth, rgbFrame.SoftwareBitmap.PixelWidth, rouMidPoint) > 0)
        {
            nearestDepthCoord = rouMidPoint;
            depthCoordFounded = true;
        }

        for (int currRadius = 1; !depthCoordFounded; currRadius++)
        {
            Vector2Int point_0 = new Vector2Int(rouMidPoint.x - currRadius, rouMidPoint.y - currRadius);

            Vector2Int startPoint;
            Vector2Int endPoint = point_0;
            for (int i = 0; i < 4 && !depthCoordFounded; i++)
            {
                startPoint = endPoint;
                endPoint = new Vector2Int(rouMidPoint.x + ((i == 0 || i == 1) ? 1 : -1) * currRadius,
                                           rouMidPoint.y + ((i == 1 || i == 2) ? 1 : -1) * currRadius);

                Vector2Int dir = endPoint - startPoint;
                dir.x = Math.Sign(dir.x);
                dir.y = Math.Sign(dir.y);
                for (Vector2Int curr = startPoint; curr != endPoint; curr += dir)
                {
                    if (curr.x < 0 || curr.x >= rgbFrame.SoftwareBitmap.PixelWidth || curr.y < 0 || curr.y >= rgbFrame.SoftwareBitmap.PixelHeight)
                        continue;

                    if (GetPixelDepth(mappedDepth, rgbFrame.SoftwareBitmap.PixelWidth, curr) > 0)
                    {
                        nearestDepthCoord = curr;
                        depthCoordFounded = true;
                        break;
                    }
                }
            }
        }

        if (!depthCoordFounded)
        {
            lc_update.LogMessage("Point with depth not founded", LoggingLevel.Information);
            return Vector3.positiveInfinity;
        }

        if (rgbFrame.CoreCameraIntrinsics == null)
        {
            lc_update.LogMessage("rgbFrame.CoreCameraIntrinsics is null", LoggingLevel.Error);
            return Vector3.positiveInfinity;
        }

        lc_update.LogMessage("On screen with depth point: " + nearestDepthCoord, LoggingLevel.Information);
        System.Numerics.Vector2 xy;
        xy = rgbFrame.CoreCameraIntrinsics.UnprojectAtUnitDepth(new Point(nearestDepthCoord.x, nearestDepthCoord.y));
        lc_update.LogMessage("On plane point: " + xy, LoggingLevel.Information);
        Vector3 pointOnUnitPlane = new Vector3(xy.X, xy.Y, 1);

        lc_update.LogMessage("Received depth for point: " + GetPixelDepth(mappedDepth, rgbFrame.SoftwareBitmap.PixelWidth, nearestDepthCoord), LoggingLevel.Information);

        Vector3 point = pointOnUnitPlane.normalized * ((float)GetPixelDepth(mappedDepth, rgbFrame.SoftwareBitmap.PixelWidth, nearestDepthCoord) / 1000);
        lc_update.LogMessage("In cloud point: <" + point.x + ", " + point.y + ", " + point.z + ">", LoggingLevel.Information);
        Vector4 pointToTransform = new Vector4(point.x, point.y, point.z, 1);

        var locator = SpatialLocator.GetDefault();
        if (locator == null)
        {
            lc_update.LogMessage("locator is null", LoggingLevel.Error);
            return Vector3.positiveInfinity;
        }

        var frame = locator.CreateStationaryFrameOfReferenceAtCurrentLocation();
        if (frame == null)
        {
            lc_update.LogMessage("frame is null", LoggingLevel.Error);
            return Vector3.positiveInfinity;
        }

        var unityCoordinateSystem = frame.CoordinateSystem;
        if (unityCoordinateSystem == null)
        {
            lc_update.LogMessage("unityCoordinateSystem is null", LoggingLevel.Error);
            return Vector3.positiveInfinity;
        }

        Matrix4x4? camToUnity = rgbFrame.FrameCoordinateSystem.TryGetTransformTo(unityCoordinateSystem);
        if (camToUnity == null)
        {
            lc_update.LogMessage("camToUnity is null", LoggingLevel.Error);
            return Vector3.positiveInfinity;
        }
        lc_update.LogMessage("camToUnity matrix: " + (Matrix4x4)camToUnity, LoggingLevel.Information);

        Matrix4x4 viewToCamera;
        if (!Matrix4x4.Invert(rgbFrame.CameraViewTransform, out viewToCamera))
        {
            lc_update.LogMessage("viewToCamera is null", LoggingLevel.Error);
            return Vector3.positiveInfinity;
        }
        lc_update.LogMessage("viewToCamera matrix: " + viewToCamera, LoggingLevel.Information);

        lc_update.LogMessage("FrameToOrigin matrix: " + rgbFrame.FrameToOrigin, LoggingLevel.Information);

        var viewToUnity = Matrix4x4.Transpose(viewToCamera * (Matrix4x4)camToUnity); 
        viewToUnity.M31 *= -1;
        viewToUnity.M32 *= -1;
        viewToUnity.M33 *= -1;
        viewToUnity.M34 *= -1;
        lc_update.LogMessage("viewToUnity matrix: " + viewToUnity, LoggingLevel.Information);

        Vector4 resultPoint = vecDotM(pointToTransform, viewToUnity);
        lc_update.LogMessage("In world point before normalizing: " + resultPoint, LoggingLevel.Information);

        return new Vector3(resultPoint.X / resultPoint.W, resultPoint.Y / resultPoint.W, resultPoint.Z / resultPoint.W);
    }

    unsafe ushort GetPixelDepth(ushort* depth, int width, Vector2Int pos)
    {
        return depth[pos.y * width + pos.x];
    }

    Vector4 vecDotM(Vector4 vec, Matrix4x4 m)
    {
        Vector4 res;
        res.X = vec.X * m.M11 + vec.Y * m.M21 + vec.Z * m.M31 + vec.W * m.M41;
        res.Y = vec.X * m.M12 + vec.Y * m.M22 + vec.Z * m.M32 + vec.W * m.M42;
        res.Z = vec.X * m.M13 + vec.Y * m.M23 + vec.Z * m.M33 + vec.W * m.M43;
        res.W = vec.X * m.M14 + vec.Y * m.M24 + vec.Z * m.M34 + vec.W * m.M44;
        return res;
    }
#endif

    async Task StopHoloLensMediaFrameSourceGroup()
    {
#if ENABLE_WINMD_SUPPORT
        if (!inited)
            return;

        // Wait for frame source groups to stop.
        await mediaFrameSourceGroupRGB.StopAsync();
        mediaFrameSourceGroupRGB = null;

        await mediaFrameSourceGroupShortDepth.StopAsync();
        mediaFrameSourceGroupShortDepth = null;

        await mediaFrameSourceGroupLongDepth.StopAsync();
        mediaFrameSourceGroupLongDepth = null;

        // Set to null value
        sensorFrameStreamerRGB = null;
        sensorFrameStreamerShortDepth = null;
        sensorFrameStreamerLongDepth = null;
#endif
    }


    #region ComImport
    // https://docs.microsoft.com/en-us/windows/uwp/audio-video-camera/imaging
    [ComImport]
    [Guid("5B0D3235-4DBA-4D44-865E-8F1D0E4FD04D")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    unsafe interface IMemoryBufferByteAccess
    {
        void GetBuffer(out byte* buffer, out uint capacity);
    }
    #endregion

#if ENABLE_WINMD_SUPPORT
    // Get byte array from software bitmap.
    // https://github.com/qian256/HoloLensARToolKit/blob/master/ARToolKitUWP-Unity/Scripts/ARUWPVideo.cs
    unsafe byte* GetByteArrayFromSoftwareBitmap(SoftwareBitmap sb)
    {
        if (sb == null)
            return null;

        SoftwareBitmap sbCopy = new SoftwareBitmap(sb.BitmapPixelFormat, sb.PixelWidth, sb.PixelHeight);
        Interlocked.Exchange(ref sbCopy, sb);
        using (var input = sbCopy.LockBuffer(BitmapBufferAccessMode.Read))
        using (var inputReference = input.CreateReference())
        {
            byte* inputBytes;
            uint inputCapacity;
            ((IMemoryBufferByteAccess)inputReference).GetBuffer(out inputBytes, out inputCapacity);
            return inputBytes;
        }
    }
#endif
}