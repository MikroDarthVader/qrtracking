#pragma once
#include "pch.h"
#include "DepthPvMapperNew.h"

namespace HoloLensForCV
{
	// Cache the image to camera mapping of depth sensor (only done once).
	DepthPvMapperNew::DepthPvMapperNew(HoloLensForCV::SensorFrame^ shortDepthFrame)
	{
		_imageToCameraMapping = createImageToCamMapping(shortDepthFrame);
	}

	// Get mapping of depth frame from image to camera. Performed once a session.
	cv::Mat DepthPvMapperNew::createImageToCamMapping(HoloLensForCV::SensorFrame^ shortDepthFrame) // must work
	{
		cv::Mat imageToCameraMapping = cv::Mat(
			shortDepthFrame->SoftwareBitmap->PixelHeight,
			shortDepthFrame->SoftwareBitmap->PixelWidth,
			CV_32FC2,
			cv::Scalar::all(0));
		for (int x = 0; x < shortDepthFrame->SoftwareBitmap->PixelWidth; ++x) {
			for (int y = 0; y < shortDepthFrame->SoftwareBitmap->PixelHeight; ++y) {
				Windows::Foundation::Point uv = { float(x), float(y) };
				Windows::Foundation::Point xy(0, 0);
				if (shortDepthFrame->SensorStreamingCameraIntrinsics->MapImagePointToCameraUnitPlane(uv, &xy)) {
					imageToCameraMapping.at<cv::Vec2f>(y, x) = cv::Vec2f(xy.X, xy.Y);
				}
			}
		}
		return imageToCameraMapping;
	}

	// Projects depth sensor data to PV frame and returns Mat with measured distances in mm in PV frame coordinates
	HoloLensForCV::SensorFrame^ DepthPvMapperNew::MapDepthToPV(
		HoloLensForCV::SensorFrame^ rgbFrame,
		HoloLensForCV::SensorFrame^ shortDepthFrame,
		HoloLensForCV::SensorFrame^ longDepthFrame,
		int depthRangeMin, int depthRangeMax)
	{
		int pvWidth = rgbFrame->SoftwareBitmap->PixelWidth;
		int pvHeight = rgbFrame->SoftwareBitmap->PixelHeight;

		cv::Mat res(pvHeight, pvWidth, CV_16UC1, cv::Scalar::all(0));

		cv::Mat pointCloud = get4DPointCloudFromDepth(shortDepthFrame, longDepthFrame, depthRangeMin, depthRangeMax);
		cv::Mat depthImage;
		Utils::WrapHoloLensSensorFrameWithCvMat(shortDepthFrame, depthImage);
		auto depthFrameToOrigin = shortDepthFrame->FrameToOrigin;
		auto depthCamViewTransform = shortDepthFrame->CameraViewTransform;
		auto pvFrameToOrigin = rgbFrame->FrameToOrigin;
		auto pvCamViewTransform = rgbFrame->CameraViewTransform;
		auto pvCamProjTransform = rgbFrame->CameraProjectionTransform;
		Windows::Foundation::Numerics::float4x4 depthCamViewTransformInv;
		Windows::Foundation::Numerics::float4x4 pvFrameToOriginInv;

		if (!Windows::Foundation::Numerics::invert(depthCamViewTransform, &depthCamViewTransformInv) ||
			!Windows::Foundation::Numerics::invert(pvFrameToOrigin, &pvFrameToOriginInv))
		{
			dbg::trace(L"Can't map depth to pv, invalid transform matrices");
			return nullptr;
		}
		// build point cloud -> pv view transform matrix
		auto depthPointToWorld = depthCamViewTransformInv * depthFrameToOrigin;
		auto depthPointToPvFrame = depthPointToWorld * pvFrameToOriginInv;
		auto depthPointToCamView = depthPointToPvFrame * pvCamViewTransform;
		auto depthPointToImage = depthPointToCamView * pvCamProjTransform;

		// loop through point cloud and estimate coordinates
		for (int x = 0; x < pointCloud.cols; ++x) {
			for (int y = 0; y < pointCloud.rows; ++y) {
				cv::Vec4f point = pointCloud.at<cv::Vec4f>(y, x);
				if (point.val[0] == 0 && point.val[1] == 0 && point.val[2] == 0)
					continue;

				// project point and normalize by final w coordinate
				cv::Vec4f projPoint = Utils::vecDotM(point, depthPointToImage);
				cv::Vec3f normProjPoint = cv::Vec3f(
					projPoint.val[0] / projPoint.val[3],
					projPoint.val[1] / projPoint.val[3],
					projPoint.val[2] / projPoint.val[3]);

				// convert point with central origin and y axis up to pv image coordinates
				// scale for screen resolution
				if (normProjPoint.val[0] > -1 && normProjPoint.val[0] < 1 && normProjPoint.val[1] > -1 && normProjPoint.val[1] < 1)
				{
					int imgX = (int)round(pvWidth * ((normProjPoint.val[0] + 1) / 2.));
					int imgY = (int)round(pvHeight * (1 - ((normProjPoint.val[1] + 1) / 2.)));

					if (res.at<ushort>(imgY, imgX) == 0 || res.at<ushort>(imgY, imgX) > depthImage.at<ushort>(y, x))
						res.at<ushort>(imgY, imgX) = depthImage.at<ushort>(y, x);
				}
			}
		}

		return Utils::WrapCvMatWithHoloLensSensorFrame(res, shortDepthFrame->Timestamp);;
	}

	// Get the 4D point cloud from depth sensor frame
	cv::Mat DepthPvMapperNew::get4DPointCloudFromDepth(HoloLensForCV::SensorFrame^ shortDepthFrame, // must work
		HoloLensForCV::SensorFrame^ longDepthFrame,
		int depthRangeMin, int depthRangeMax)
	{
		cv::Mat depthImage;
		Utils::WrapHoloLensSensorFrameWithCvMat(shortDepthFrame, depthImage);
		cv::Mat pointCloud(depthImage.rows, depthImage.cols, CV_32FC4, cv::Scalar::all(0));
		for (int x = 0; x < depthImage.cols; ++x) {
			for (int y = 0; y < depthImage.rows; ++y) {

				// Make sure point cloud is within specified depth range
				if (depthImage.at<unsigned short>(y, x) < depthRangeMin || depthImage.at<unsigned short>(y, x) > depthRangeMax)
					continue;

				auto camPoint = _imageToCameraMapping.at<cv::Vec2f>(y, x);
				cv::Point3f d(camPoint.val[0], camPoint.val[1], 1);

				// scale factor for depth point cloud
				// (-1 * depth(y,x) / 1000) * (1 / sqrt(x^2 + y^2 + 1)
				d *= -(depthImage.at<unsigned short>(y, x) / 1000.0) * (1 / sqrt(d.x * d.x + d.y * d.y + 1));
				pointCloud.at<cv::Vec4f>(y, x) = cv::Vec4f(d.x, d.y, d.z, 1);
			}
		}
		return pointCloud;
	}
}
