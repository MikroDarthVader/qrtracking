#pragma once

// DepthPvMapper implementation from:
// https://github.com/cyberj0g/HoloLensForCV/blob/master/Samples/ComputeOnDevice/DepthPvMapper.cpp

namespace HoloLensForCV
{
	//
	// Creates a synchronized mapping between
	// depth and photo video frames.
	//
	public ref class DepthPvMapperNew sealed
	{
	public:
		// Initialize depth image space coordinates to unit plane mapping 
		// (reverse depth cam space projection transform, 2D->3D)
		// this transform doesn't depend on actual depth values 
		// and can be done once per sensor stream activation
		DepthPvMapperNew(HoloLensForCV::SensorFrame^ shortDepthFrame);

		// Need to return a windows runtime type by public member.
		HoloLensForCV::SensorFrame^ MapDepthToPV(
			HoloLensForCV::SensorFrame ^ rgbFrame,
			HoloLensForCV::SensorFrame ^ shortDepthFrame,
			HoloLensForCV::SensorFrame^ longDepthFrame,
			int depthRangeMin,
			int depthRangeMax);

	private:

		// Using custom built Nuget package for
		// OpenCV 4.1.1 Windows compile.
		cv::Mat _imageToCameraMapping;
		cv::Mat createImageToCamMapping(HoloLensForCV::SensorFrame^ shortDepthFrame);
		cv::Mat get4DPointCloudFromDepth(HoloLensForCV::SensorFrame^ shortDepthFrame, 
										 HoloLensForCV::SensorFrame^ longDepthFrame, 
									     int depthRangeMin, 
										 int depthRangeMax);
	};
}

